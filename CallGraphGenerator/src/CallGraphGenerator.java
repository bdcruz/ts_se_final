import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;


public class CallGraphGenerator {
	
	public static ArrayList<File> fileList = new ArrayList<File>();
    
    public static void listf(String directoryName, ArrayList<File> files) {
        
    	File directory = new File(directoryName);
        File[] fList = directory.listFiles();
        for (File file : fList) {
            
        	if (file.isFile()) {
            
            	files.add(file);
                fileList.add(file);
            
            } else if (file.isDirectory()) {
            
            	listf(file.getAbsolutePath(), files);
            
            }
        }
    }
    
    public static float equalMaps(Map<String, List<String>>m1, Map<String, List<String>>m2) {
    	   /*if (m1.size() != m2.size()){
    	      return false;
    	   }*/
    	float total = 0;
    	float hits = 0;
    	
    	System.out.println(m1.size() + " " + m2.size());
    	
    	if (m1.size() >= m2.size()){
    		
    		for (String key: m1.keySet()){
    
    			total++;
    			if (m1.get(key).equals(m2.get(key))){
    				//System.out.println(m1.get(key) + " @@ " + m2.get(key));
    				System.out.println(m1.get(key));
    				hits++;
    			}
    		}
    	
    	} else {
    		
    		for (String key: m2.keySet()){
    		    
    			total++;
    			if (m2.get(key).equals(m1.get(key))){
    				//System.out.println(m1.get(key) + " @@ " + m2.get(key));
    				System.out.println(m1.get(key));
    				hits++;
    			}
    		}
    		
    	}
    	return (hits/total);
    }
    
    
    public static Map<String, List<String>> getHashMap(String filepath) throws IOException, SAXException, ParserConfigurationException{
    	
    	//listf(".", new ArrayList<File>());
    	listf(filepath, new ArrayList<File>());
		
    	Map<String, List<String>> callGraph_return = new HashMap<String, List<String>>();
		
    	for (int i = 0; i < fileList.size(); i++) {
           try{ 
            if(!fileList.get(i).getName().contains(".DS_Store")){
            	if(fileList.get(i).getName().contains(".xml")){
            		
            		String srcmlFilename = fileList.get(i).getAbsolutePath().replace("/.", "");
            		//System.out.println(srcmlFilename);
            		SRCMLParser p = new SRCMLParser(srcmlFilename);
            		
            		Map<String, List<String>> callGraph = p.getCallGraph();
            		if(callGraph != null){
            		
            			for (String name: callGraph.keySet()){
            				try{
            					callGraph_return.put(name, callGraph.get(name));
            					//String key = name.toString();
            					//String value = callGraph.get(name).toString();  
            					//System.out.println(key + " " + value);  
            				}catch(Exception e){}
            			}
            		}
            	}
            }
           }catch(Exception e){}
        }
    	
    	return callGraph_return;
    }
	
	public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException {
	
		Map<String, List<String>> callGraph1 = getHashMap(args[0]);
		Map<String, List<String>> callGraph2 = getHashMap(args[1]);
		System.out.println(equalMaps(callGraph1, callGraph2));
	}
}

