import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class FileRetriever {
    
    
    public static ArrayList<File> fileList = new ArrayList<File>();
    
    public static void listf(String directoryName, ArrayList<File> files) {
        File directory = new File(directoryName);
        
        // get all the files from a directory
        File[] fList = directory.listFiles();
        for (File file : fList) {
            if (file.isFile()) {
                files.add(file);
                fileList.add(file);
            } else if (file.isDirectory()) {
                listf(file.getAbsolutePath(), files);
            }
        }
    }
    
    public static void main(String[] args) throws InterruptedException, IOException {
        
        listf(".", new ArrayList<File>());
        
        
        /*
        for (int i = 0; i < fileList.size(); i++) {
         
         if(!fileList.get(i).getName().contains(".DS_Store"))
             System.out.println(fileList.get(i).getAbsolutePath());
         
         }
        */
        for (int i = 0; i < fileList.size(); i++) {
            
            //if(!fileList.get(i).getName().contains(".DS_Store")){
                String command = ("./srcml " + fileList.get(i).getAbsolutePath().replace("/.", "") + " -o " + fileList.get(i).getAbsolutePath().replace(".java", ".xml"));
                Process proc = Runtime.getRuntime().exec(command);
                proc.waitFor();
          //  }
        }
        
    }
}
