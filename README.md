# README #

This repository contains all the data and applications used for the final project

### What is this repository for? ###
This repository is for storing the created code used for the final project
CallGraph generator generates the call graphs of two distinct applications and 
calculates the similarity values.
Remember to not provide java files to CallGraphGenerator

	CallGraphGenerator input: 2 java projects parsed using srcml
	CallGraphGenerator output: call graph and similarity value

CallGraphGenerator usage
$ java CallGraphGenerator <PATH TO 1st XML APPLICATION> <PATH TO 2nd XML APPLICATION>

Processing/FileRetriever
This application parses all java files from a folder and runs srcml
The resulting project files are placed in the same folder as the project

similarities.xlsx contains the similarity values package names and categories used in this study.